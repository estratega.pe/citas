<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>Login</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <link href="{BASE_URL}assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
   <link href="{BASE_URL}assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
   <link href="{BASE_URL}assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
   <link href="{BASE_URL}css/style.css" rel="stylesheet" />
   <link href="{BASE_URL}css/style-responsive.css" rel="stylesheet" />
   <link href="{BASE_URL}css/style-default.css" rel="stylesheet" id="style_color" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="lock">
    <div class="lock-header">
        <!-- BEGIN LOGO -->
        <a class="center" id="logo" href="{BASE_URL}">
            <img class="center" alt="logo" src="{BASE_URL}img/logo-front.png">
        </a>
        <!-- END LOGO -->
    </div>
    <div class="login-wrap">
        <div class="metro single-size yellow">
            <div class="locked">
                <i class="icon-lock"></i>
                <span>Ingrese Email</span>
            </div>
        </div>
        <form action="{URL_POST}" method="post">
	        <div class="metro double-size {BG_COLOR}">
	        <span>{BODY_SUBTITLE}</span>
				<div class="input-append lock-input">
					{INPUT_USER_EMAIL}
				</div>
	        </div><!-- 
	        <div class="metro double-size yellow">
				<div class="input-append lock-input">
					<input type="password" name="contrasena" class="" placeholder="Ingrese Contrase&ntilde;a" required>
				</div>
	        </div>-->
	        <div class="metro double-size terques login">
				<button type="submit" class="btn login-btn">
					Recuperar Clave
					<i class=" icon-long-arrow-right"></i>
				</button>
	        </div>
        </form>
        <div class="metro single-size blue ">
            <a href="{BASE_URL}login/" class="social-link">
                <i class="icon-key"></i>
                <span>Volver al login</span>
            </a>
        </div>
        <div class="metro double-size navy-blue ">
            <a href="{BASE_URL}login/register" class="social-link">
                <i class="icon-edit"></i>
                <span>Registrarme</span>
            </a>
        </div>
        
        <div class="metro double-size deep-red">
                <p><span>Portal para gesti&oacute;n de citas</span></p>
                <p>Permite tener una agenda de todas las citas o atenciones a sus clientes, genera alertas en forma de correos, para recordarle al cliente, al trabajador y al administrador las citas agendadas.</p>
                <p>Permite administrar las citas de varias empresas y locales</p>
        </div>
        <div class="metro double-size purple text-left">
                <p><span>M&oacute;dulos adicionales:</span></p>
                <ul>
                	<li>Planes de atenci&oacute;n</li>
                	<li>Facturaci&oacute;n Electr&oacute;nica</li>
                	<li>Caja Cobranza y Caja Chica</li>
                	<li>Control de Asistencia</li>
                </ul>
        </div>
        <!--
        <div class="metro double-size blue">
            <a href="index.html" class="social-link">
                <i class="icon-twitter-sign"></i>
                <span>Twitter Login</span>
            </a>
        </div>
        <div class="metro single-size purple">
            <a href="index.html" class="social-link">
                <i class="icon-skype"></i>
                <span>Skype Login</span>
            </a>
        </div>-->
        <div class="login-footer">
			<!-- 
			<div class="remember-hint pull-left">
                  <a href="{BASE_URL}login/register" class="social-link">Registrarme</a>
            </div>
            
            <div class="forgot-hint pull-right">
                <a id="forget-password" class="" href="{BASE_URL}login/recoveryPassword">Olvid&oacute; su contrase&ntilde;a?</a>
            </div>
            -->
        </div>
    </div>
</body>
<!-- END BODY -->
</html>