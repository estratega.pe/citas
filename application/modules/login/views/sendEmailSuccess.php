<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>{BODY_TITLE}</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
	<link href="{BASE_URL}assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="{BASE_URL}assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link href="{BASE_URL}assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
    <link href="{BASE_URL}assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="{BASE_URL}css/style.css" rel="stylesheet" />
    <link href="{BASE_URL}css/style-responsive.css" rel="stylesheet" />
    <link href="{BASE_URL}css/style-default.css" rel="stylesheet" id="style_color" />

    <link href="{BASE_URL}assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{BASE_URL}assets/uniform/css/uniform.default.css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="lock">
    <div class="lock-header">
        <!-- BEGIN LOGO -->
        <a class="center" id="logo" href="{BASE_URL}">
            <img class="center" alt="logo" src="{BASE_URL}img/logo-front.png">
        </a>
        <!-- END LOGO -->
    </div>
        
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget box green">
                        <div class="widget-title">
                            <h4>
                                <i class="icon-reorder"></i> <span>{BODY_SUBTITLE}</span>
                            </h4>
                        <span class="tools">

                        </span>
                        </div>
                        <div class="widget-body">
                            <span>{BODY_TEXT}</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
          </div>
<!-- BEGIN FOOTER -->
   <div id="footer">
       <!-- 2016 - <?php //echo date('Y');?>&copy; www.jyconsultores.com -->
   </div>
   <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="{BASE_URL}js/jquery-1.8.3.min.js"></script>
   <script src="{BASE_URL}js/jquery.nicescroll.js" type="text/javascript"></script>
   <script src="{BASE_URL}assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="{BASE_URL}assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="{BASE_URL}js/jquery.blockui.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="{BASE_URL}js/excanvas.js"></script>
   <script src="{BASE_URL}js/respond.js"></script>
   <![endif]-->
   <script type="text/javascript" src="{BASE_URL}assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="{BASE_URL}assets/uniform/jquery.uniform.min.js"></script>
   <script src="{BASE_URL}js/jquery.scrollTo.min.js"></script>

   <!--common script for all pages-->
   <script src="{BASE_URL}js/common-scripts.js"></script>
        		
</body>
<!-- END BODY -->
</html>