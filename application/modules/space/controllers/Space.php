<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Space extends MX_Controller {
	public  $UID;
	public $UTYPE;
	function __construct()
	{
		parent::__construct();
		$this->load->model('space/spaceModel', 'space');
		$this->UID = $this->session->userdata('USER')->user_id;
		$this->UTYPE = $this->session->userdata('USER')->user_type;
	}
	
	public function index()
	{
		$columnas = [
				['LABEL' => 'ID']
				,['LABEL' => 'ESPACIO']
				,['LABEL' => 'LOCAL']
				,['LABEL' => 'EMPRESA']
				,['LABEL' => 'ACCIONES']
		];
		
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Listado de Espacios', 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('listGridAjax', ['BASE_URL' => base_url()
			,'BODY_TITLE'		=> 'Espacios'
			,'URL_AJAX'			=> base_url() . 'space/getSpace/datatables'
			,'BODY_DESCRIPTION'	=> 'Listado de Espacios'
			,'BODY_SUBTITLE'	=> ''
			,'TARGETS'			=> count($columnas)
			,'ID_TARGET'		=> 'space_id'
			,'COLUMNAS'			=> json_encode($columnas)
			,'BODY_MENU'		=> createLink(base_url() . 'space/addSpace', 'btn-blue-alt', 'icon-plus', 'Nuevo', true)
			,'TH_TABLE'			=> $columnas
		], TRUE);
		renderPage($data);
	}
	
	public function addSpace() {
		$subtitle = 'Crear nuevo espacio';
		$bg_color = 'blue';
		$local_id = '';
		$space_name = '';
		$company_id = '';
		if( $this->input->post() ) {
			$space_name = $this->input->post('input_space_name', TRUE);
			$local_id = $this->input->post('select_local_id');
			$company_id = $this->input->post('select_company_id');
			if( empty($local_id) || empty($space_name) || empty($company_id) ) {
				$subtitle = 'Datos erroneos, intentelo nuevamente.';
				$bg_color = 'red';
			} else {
				$data = [
						'space_name'	=> $space_name
						,'local_id'		=> $local_id
						,'company_id'	=> $company_id
				];
				$newID = $this->gen->saveData('jyc_space', $data);
				if(  is_numeric($newID) ) {
					redirect(base_url() . 'space/viewSpace/' . $newID);
				} else {
					$subtitle = 'Lo sentimos en este momento no podemos procesar su solicitud, por favor intentelo m&aacute;s tarde.';
					$bg_color = 'red';
				}
			}
			
		}
		$select_company_id = ['' => 'Seleccione'];
		$c = $this->company->getCompany(['user_id' => $this->UID, 'company_status' => 1, 'user_type' => $this->config->item('IND_ADMIN')]);
		foreach($c as $k) {
			$select_company_id[$k->company_id] = $k->company_social;
		}
		$select_local_id = [];
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Nuevo Espacio', 'BASE_URL' => base_url()];
		$data['BODY_TITLE'] = 'Nuevo Espacio';
		$data['BODY'] = $this->parser->parse('space/addSpace', ['BASE_URL' => base_url()
			,'BODY_TITLE'			=> 'Nuevo Espacio'
			,'BODY_SUBTITLE'		=> $subtitle
			,'BODY_MENU'			=> ''
			,'BG_COLOR'				=> $bg_color
			,'URL_POST'				=> base_url() .'space/addSpace/'
			,'INPUT_SPACE_NAME'		=> form_input(array('name' => 'input_space_name', 'id' => 'input_space_name', 'maxlength' => '60'), $space_name, 'class="span6" required ')
			,'SELECT_LOCAL_ID'		=> form_dropdown(array('name' => 'select_local_id', 'id' => 'select_local_id'), $select_local_id, $local_id, 'class="span6" disabled required')
			,'SELECT_COMPANY_ID'	=> form_dropdown(array('name' => 'select_company_id', 'id' => 'select_company_id'), $select_company_id, $company_id, 'class="span6" required')
			,'BUTTON_SUBMIT'		=> createSubmitButton('Crear', 'btn-success', 'icon-save')
			,'BUTTON_CANCEL'		=> createLink(base_url() . 'space', ' ', 'icon-ban', 'Cancelar', true)
		], TRUE);
		$data['FOOTER_EXTRA'] = "
				<script>
				$('#select_company_id').change(function() {
					ID = $(this).val();
					if(ID >= 1) {
						$.ajax({
						  url: '" . base_url() . "space/getLocalOptions/' + ID,
						  dataType: 'html'
						}).done(function(row){
							$('#select_local_id').html(row).attr('disabled', false);
						});
					} else {
						$('#select_local_id').html('').attr('disabled', true);
					}
					
				});
				</script>
		";
		renderPage($data);
	}
	
	public function editSpace() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000 ) {
			header('Location: ' . base_url() . 'space');
			exit();
		}
		
		$bg_color = 'blue';
		$where = [
				'rel_company_user.user_id'		=> $this->UID
				,'rel_company_user.user_type'	=> $this->UTYPE
				,'space_id'						=> $ID
		];
		$c = $this->space->getRowSpace($where);
		if(count($c) <= 0x0000) {
			redirect(base_url() . 'space');
			exit();
		}
		$subtitle = 'Editar local';
		$local_id = $c->local_id;
		$company_id = $c->company_id;
		$space_name = $c->space_name;
		if( $this->input->post() ) {
			$local_id = $this->input->post('select_local_id');
			$company_id = $this->input->post('select_company_id');
			$space_name = $this->input->post('input_space_name', TRUE);
			if( empty($local_id) || empty($space_name) || empty($company_id) ) {
				$subtitle = 'Datos erroneos, intentelo nuevamente.';
				$bg_color = 'red';
			} else {
				$data = [
						'company_id'	=> $company_id
						,'local_id'		=> $local_id
						,'space_name'	=> $space_name
				];
				$where = [
						'space_id'	=> $ID
				];
				if(  !$this->gen->updateData('jyc_space', $data, $where) ) {
					$subtitle = 'Lo sentimos en este momento no podemos procesar su solicitud, por favor intentelo m&aacute;s tarde.';
					$bg_color = 'red';
				} else {
					redirect(base_url() . 'space/viewSpace/' . $ID);
				}
			}
				
		}
		$select_company_id = ['' => 'Seleccione'];
		$c = $this->company->getCompany(['user_id' => $this->UID, 'company_status' => 1, 'user_type' => $this->config->item('IND_ADMIN')]);
		foreach($c as $k) {
			$select_company_id[$k->company_id] = $k->company_social;
		}
		$select_local_id = [];
		$local = $this->gen->getData('jyc_local', ['company_id' => $company_id, 'local_status' => 1]);
		foreach($local as $l) {
			$select_local_id[$l->local_id] = $l->local_name;
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Editar Espacio', 'BASE_URL' => base_url()];
		$data['BODY_TITLE'] = 'Editar espacio';
		$data['BODY'] = $this->parser->parse('space/editSpace', ['BASE_URL' => base_url()
				,'BODY_TITLE'			=> 'Editar Espacio'
				,'BODY_SUBTITLE'		=> $subtitle
				,'BODY_MENU'			=> ''
				,'BG_COLOR'				=> $bg_color
				,'URL_POST'				=> base_url() .'space/editSpace/' . $ID
				,'INPUT_SPACE_NAME'		=> form_input(array('name' => 'input_space_name', 'id' => 'input_space_name', 'maxlength' => '60'), $space_name, 'class="span6" required ')
				,'SELECT_LOCAL_ID'		=> form_dropdown(array('name' => 'select_local_id', 'id' => 'select_local_id'), $select_local_id, $local_id, 'class="span6" required')
				,'SELECT_COMPANY_ID'	=> form_dropdown(array('name' => 'select_company_id', 'id' => 'select_company_id'), $select_company_id, $company_id, 'class="span6" required')
				,'BUTTON_SUBMIT'		=> createSubmitButton('Actualizar', 'btn-success', 'icon-save')
				,'BUTTON_CANCEL'		=> createLink(base_url() . 'space', ' ', 'icon-ban', 'Cancelar', true)
		], TRUE);
		$data['FOOTER_EXTRA'] = "
				<script>
				$('#select_company_id').change(function() {
					ID = $(this).val();
					if(ID >= 1) {
						$.ajax({
						  url: '" . base_url() . "space/getLocalOptions/' + ID,
						  dataType: 'html'
						}).done(function(row){
							$('#select_local_id').html(row).attr('disabled', false);
						});
					} else {
						$('#select_local_id').html('').attr('disabled', true);
					}
			
				});
				</script>
		";
		renderPage($data);
	}
	
	public function viewSpace() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		if($ID == 0x0000 ) {
			header('Location: ' . base_url() . 'space');
			exit();
		}
		$subtitle = 'Ver Espacio';
		$bg_color = 'blue';
		$where = [
				'rel_company_user.user_id'		=> $this->UID
				,'rel_company_user.user_type'	=> $this->UTYPE
				,'space_id'						=> $ID
		];
		$c = $this->space->getRowSpace($where);
		if(count($c) <= 0x0000) {
			redirect(base_url() . 'space');
			exit();
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Ver Espacio', 'BASE_URL' => base_url()];
		$data['BODY_TITLE'] = 'Ver Espacio';
		$data['BODY'] = $this->parser->parse('space/viewSpace', ['BASE_URL' => base_url()
				,'BODY_TITLE'				=> 'Ver Espacio'
				,'BODY_SUBTITLE'			=> $subtitle
				,'BODY_MENU'				=> ''
				,'BG_COLOR'					=> $bg_color
				,'INPUT_DES_NOMBRE'			=> $c->space_name
				,'SELECT_LOCAL_ID'			=> $c->local_name
				,'SELECT_COMPANY_ID'		=> $c->company_social
				,'LINK_BACK'				=> createLink(base_url() . 'space', ' ', 'icon-ban', 'regresar al listado', true)
		], TRUE);
		renderPage($data);
	}
	
	public function deleteSpace() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		$bg_color = '';
		if( $ID == 0x0000 ) {
			header('Location: ' . base_url() . 'local');
			exit();
		}
		$where = [
				'rel_company_user.user_id'		=> $this->UID
				,'rel_company_user.user_type'	=> $this->UTYPE
				,'space_id'						=> $ID
		];
		$c = $this->space->getRowSpace($where);
		if(count($c) <= 0x0000) {
			redirect(base_url() . 'space');
			exit();
		}
		$subtitle = 'No se puede deshacer la eliminaci&oacute;n';
		if($this->input->post()) {
			if( $this->input->post('input_space_id') == $ID) {
				$data = [
						'space_status'	=> 0
				];
				$where = [
						'space_id'	=> $this->input->post('input_space_id')
				];
				if (!$this->gen->updateData('jyc_space', $data, $where) ) {
					$this->db->trans_rollback();
					$subtitle = 'Lo sentimos en este momento no podemos procesar su solicitud, por favor intentelo m&aacute;s tarde.';
					$bg_color = 'red';
				} else {
					redirect(base_url() . 'space');
				}
			}
		}
		$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'Eliminar Espacio ' . $c->space_name, 'BASE_URL' => base_url()];
		$data['BODY'] = $this->parser->parse('deleteForm', ['BASE_URL' => base_url()
			,'BODY_TITLE'				=> 'Eliminar Espacio - ' . $c->space_name
			,'BODY_SUBTITLE'			=> $subtitle
			,'BODY_MENU'				=> '(' . $c->space_name . ')'
			,'BODY_DESCRIPTION'			=> 'Confirme eliminacion del espacio "' . $c->space_name . '"'
			,'BG_COLOR'					=> $bg_color
			,'URL_POST'					=> base_url() . 'space/deleteSpace/' . $ID
			,'INPUT_DELETE_ID'			=> form_hidden('input_space_id', $ID)
			,'BUTTON_SUBMIT'			=> createSubmitButton('Eliminar', 'btn-success', 'icon-remove')
			,'BUTTON_CANCEL'			=> createLink(base_url() . 'space', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		renderPage($data);
	}
	
	public function recoverySpace() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		$bg_color = '';
		if( $ID == 0x0000 ) {
			header('Location: ' . base_url() . 'company');
			exit();
		}
		$where = [
				'rel_company_user.user_id'		=> $this->UID
				,'rel_company_user.user_type'	=> $this->UTYPE
				,'space_id'						=> $ID
		];
		$c = $this->space->getRowSpace($where);
		if(count($c) >= 0x0001 && ($c->space_id == $ID) ) {
			$data = [
					'space_status'	=> 1
			];
			$where = [
					'space_id'	=> $ID
			];
			if (!$this->gen->updateData('jyc_space', $data, $where) ) {
				$this->db->trans_rollback();
			} else {
					
			}
		}
	
		redirect(base_url() . 'space');
	}
	
	public function getSpace() {
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$_UID = ($this->UTYPE == $this->config->item('IND_ADMIN')) ? $this->UID : getAdminUID();
		$where = [
				'rel_company_user.user_id'		=> $_UID
				//,'jyccompany.ind_status'	=> 1 
		];
		if( $this->input->post() ) {
			$total = 0;
			$start = $this->input->post('iDisplayStart');
			$limit = $this->input->post('iDisplayLength');
			$sEcho = $this->input->post('sEcho');
			if($this->input->post('sSearch', TRUE) != '') {
				$search = $this->input->post('sSearch');
				$like = ['space_name' => $search];
				$result = $this->space->getSpace($where, $like, $limit, $start);
				$total = $this->space->getTotalSpace($where, $like);
			} else {
				if($this->input->post('q') != '') {
					$search = $this->input->post('q');
					$like = ['space_name' => $search];
					$result = $this->space->getSpace($where, $like, $limit, $start);
					$total = $this->space->getTotalSpace($where, $like);
				} else {
					$result = $this->space->getSpace($where, null, $limit, $start);
					$total = $this->space->getTotalSpace($where);
				}
			}
		} else {
			$result = $this->space->getSpace($where);
		}
		switch($format) {
			case 'object':
				return $result;
			break;
			case 'array':
				
			break;
			case 'datatables':
				$data = [];
				if($this->input->post()) {
					$records = [];
					foreach($result as $r) {
						$show = createLink(base_url() . 'space/viewSpace/' . $r->space_id, 'btn-info', 'icon-eye-open', 'Ver');
						$edit = '&nbsp;' . createLink(base_url() . 'space/editSpace/' . $r->space_id, 'btn-success', 'icon-pencil', 'Editar');
						$del = '&nbsp;' . createLink(base_url() . 'space/deleteSpace/' . $r->space_id, 'btn-danger', 'icon-trash', 'Eliminar');
						$link = $show . $edit . $del;
						if($r->local_status == 0x0001) {
							$rowClass = '';
							$local = $r->local_name;
						} else {
							$link = 'Recupere el local primero';
							$rowClass = 'text-error';
							$local = 'LOCAL ELIMINADO';
							$empresa = '';
						}
						if($rowClass != 'text-error') {
							if($r->company_status == 0x0001) {
								$empresa = $r->company_social;
							} else {
								$link = 'Recupere la empresa primero';
								$rowClass = 'text-error';
								$empresa = 'EMPRESA ELIMINADA';
							}
							if($rowClass != 'text-error') {
								if(!$r->space_status == 0x0001) {
									$rowClass = 'text-error';
									$link = createLink(base_url() . 'space/recoverySpace/' . $r->space_id, 'btn-warning', 'icon-ok', 'Recuperar');
								}
							}
						}
						
						
						array_push($records, [
							'DT_RowId'	=> $r->space_id
							,'DT_RowClass' => $rowClass
							,0	=> $r->space_id
							,1	=> $r->space_name
							,2	=> $r->local_name
							,3	=> $empresa
							,4	=> $link
						]);
					}
					$data = ['sEcho' => $sEcho
						,'iTotalRecords' => $total
						,'iTotalDisplayRecords' => $total
						,'aaData' => $records
					];
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $data ));
			break;
			default:
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $result ));
			break;
		}
		
	}
	
	public function getLocalOptions() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		$bg_color = '';
		if( $ID >= 0x0001 ) {
			$where = [
					'rel_company_user.user_id'		=> $this->UID
					,'rel_company_user.user_type'	=> $this->UTYPE
					,'rel_company_user.company_id'	=> $ID
					,'local_status'					=> 1
			];
			$local = Modules::run('local/getLocalOptions', $where);
		}	
	}
	
	public function getSpaceOptions() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(3);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		$COMPANY = (sizeof(func_get_args()) >= 0x0002) ? func_get_arg(1): $this->uri->segment(4);
		$COMPANY = filter_var(intval($COMPANY),FILTER_VALIDATE_INT) ? $COMPANY : 0;
		$html = "<option>Seleccione...</option>";
		if( $ID >= 0x0001 && $COMPANY >= 0x0001) {
			$where = [
					'local_id'		=> $ID
					,'company_id'	=> $COMPANY
					,'space_status'	=> 1
			];
			$space = $this->gen->getData('jyc_space', $where);
			foreach($space as $s) {
				$html .= "<option value='$s->space_id'>$s->space_name</option>";
			}
		}
		$this->output
		->set_content_type('application/html')
		->set_output( $html );
	}
}
