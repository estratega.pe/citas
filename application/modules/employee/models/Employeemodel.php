<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class employeeModel extends MX_Controller {
	
	const LOCAL = 'jyc_local';
	const EMPRESA = 'jyc_company';
	const SELECT = 'gen_select';
	const EMPLOYEE = 'gen_user';
	const R_C_U = 'rel_company_user';
	const R_C_E = 'rel_company_employee';
	
    function __construct()
    {
        parent::__construct();
    }
    
    function getEmployee($where = null, $like = null, $limit = null, $start = null, $or_like = null) {
    	$this->db->select(self::R_C_E . '.*,' 
    			. self::EMPRESA .'.company_social, '
    			. self::EMPRESA .'.company_status, '
    			. self::EMPLOYEE . '.*
    	');
    	$this->db->from(self::R_C_E);
    	$this->db->join(self::EMPRESA, self::EMPRESA. '.company_id = ' . self::R_C_E . '.company_id');
    	$this->db->join(self::EMPLOYEE, self::EMPLOYEE . '.user_id = ' . self::R_C_E . '.user_id');
    	$this->db->join(self::R_C_U, self::R_C_U . '.company_id = ' . self::R_C_E . '.company_id');
    	$this->db->where(self::R_C_E . '.user_type', $this->config->item('IND_EMPLOYEE'));
    	$this->db->where(self::R_C_U . '.user_type', $this->config->item('IND_ADMIN'));
    	if(!is_null($where) AND is_array($where))
    		$this->db->where($where);
    	if(!is_null($like) AND is_array($like)) {
    		$this->db->group_start();
    		$this->db->like($like);
    		if(is_null($or_like) AND !is_array($or_like))
    			$this->db->group_end();
    	}
    	if(!is_null($or_like) AND is_array($or_like)) {
    		if(is_null($like) AND !is_array($like))
    			$this->db->group_start();
    		$this->db->or_like($or_like);
    		$this->db->group_end();
    	}
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($limit, $start);
    	return $this->db->get()->result();
    }
    
    function getTotalEmployee($where = null, $like = null, $limit = null, $start = null, $or_like = null) {
    	$this->db->select(self::R_C_E . '.*,'
    			. self::EMPRESA .'.company_social, '
    			. self::EMPRESA .'.company_status, '
    			. self::EMPLOYEE . '.*
    	');
    	$this->db->from(self::R_C_E);
    	$this->db->join(self::EMPRESA, self::EMPRESA. '.company_id = ' . self::R_C_E . '.company_id');
    	$this->db->join(self::EMPLOYEE, self::EMPLOYEE . '.user_id = ' . self::R_C_E . '.user_id');
    	$this->db->join(self::R_C_U, self::R_C_U . '.company_id = ' . self::R_C_E . '.company_id');
    	$this->db->where(self::R_C_E . '.user_type', $this->config->item('IND_EMPLOYEE'));
    	$this->db->where(self::R_C_U . '.user_type', $this->config->item('IND_ADMIN'));
    	if(!is_null($where) AND is_array($where))
    		$this->db->where($where);
    	if(!is_null($like) AND is_array($like)) {
    		$this->db->group_start();
    		$this->db->like($like);
    		if(is_null($or_like) AND !is_array($or_like))
    			$this->db->group_end();
    	}
    	if(!is_null($or_like) AND is_array($or_like)) {
    		if(is_null($like) AND !is_array($like))
    			$this->db->group_start();
    		$this->db->or_like($or_like);
    		$this->db->group_end();
    	}
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($limit, $start);
    	return $this->db->count_all_results();
    }
	
	function getRowEmployee($where = null) {
		$this->db->select(self::R_C_E . '.*,'
				. self::EMPRESA .'.company_social, '
				. self::EMPRESA .'.company_status, '
				. self::EMPLOYEE . '.*
    	');
		$this->db->from(self::R_C_E);
		$this->db->join(self::EMPRESA, self::EMPRESA. '.company_id = ' . self::R_C_E . '.company_id');
		$this->db->join(self::EMPLOYEE, self::EMPLOYEE . '.user_id = ' . self::R_C_E . '.user_id');
		$this->db->join(self::R_C_U, self::R_C_U . '.company_id = ' . self::R_C_E . '.company_id');
		$this->db->where(self::R_C_E . '.user_type', $this->config->item('IND_EMPLOYEE'));
		$this->db->where(self::R_C_U . '.user_type', $this->config->item('IND_ADMIN'));
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		return $this->db->get_where()->row();
	}
	
	function saveEmployee($data, $dataRel) {
		$this->db->trans_start();
		$this->db->insert(self::EMPLOYEE, $data);
		$ID = $this->db->insert_id();
		$dataRel['user_id'] = $ID;
		$this->db->insert(self::R_C_E, $dataRel);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return $ID;
	}
	
	function updateEmployee($data, $where, $_data, $_where) {
		$this->db->trans_start();
		$this->db->update(self::EMPLOYEE, $data, $where);
		$this->db->update(self::R_P_C, $_data, $_where);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function setEmployee($data, $where, $_where) {
		$this->db->trans_start();
		$this->db->update(self::EMPLOYEE, $data, $where);
		$this->db->update(self::R_P_C, $data, $_where);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function searchEmployee($search, $company) {
		$this->db->from(self::EMPLOYEE);
		$this->db->join(self::R_C_E, self::R_C_E . '.user_id=' . self::EMPLOYEE . '.user_id');
		$this->db->where(self::R_C_E . '.user_type', $this->config->item('IND_EMPLOYEE'));
		$this->db->where(self::R_C_E . '.company_id', $company);
		$this->db->like('user_firstname', $search);
		$this->db->or_like('user_lastname', $search);
		$this->db->limit(20);
		return $this->db->get()->result();
	}
}
