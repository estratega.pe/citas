<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {
	public  $UID;
	public $UTYPE;
	function __construct()
	{
		parent::__construct();
		$this->load->model('home/homeModel', 'home');
		$this->load->model('company/companyModel', 'company');
		$this->load->model('calendar/calendarModel', 'calendar');
		
		$this->UID = $this->session->userdata('USER')->user_id;
		$this->UTYPE = $this->session->userdata('USER')->user_type;
		
	}
	
	public function index()
	{	
		$action = $this->uri->segment(3);
		
		$subtitle = 'LUGAR DE TRABAJO';
		$bg_color = 'blue';
		
		if($_POST) {
			switch ($action) {
				case 'setHome':
					if($this->UTYPE == $this->config->item('IND_ADMIN')) {
						$company_id = $this->input->post('select_company_id');
						$local_id = $this->input->post('select_local_id');
						if($this->gen->updateData('gen_user', ['company_id' => $company_id, 'local_id' => $local_id], ['user_id' => $this->UID]) === FALSE) {
							$subtitle = 'Error al guardar el LUGAR DE TRABAJO.';
							$bg_color = 'red';
						} else {
							$subtitle = 'LUGAR DE TRABAJO guardado con &eacute;xito.';
							$bg_color = 'green';
							$usr = $this->db->get_where('gen_user', ['user_id' => $this->UID])->row();
							$this->session->unset_userdata('USER');
							$this->session->set_userdata('USER', $usr);
						}
					} else {
						$local_id = $this->input->post('select_local_id');
						if($this->gen->updateData('gen_user', ['local_id' => $local_id], ['user_id' => $this->UID]) === FALSE) {
							$subtitle = 'Error al guardar el LUGAR DE TRABAJO.';
							$bg_color = 'red';
						} else {
							$subtitle = 'LUGAR DE TRABAJO guardado con &eacute;xito.';
							$bg_color = 'green';
							$usr = $this->db->get_where('gen_user', ['user_id' => $this->UID])->row();
							$this->session->unset_userdata('USER');
							$this->session->set_userdata('USER', $usr);
						}
					}
				break;
			}
		}
		
		
		$u = $this->gen->getRow('gen_user', ['user_id' => $this->UID]);
		
		$company_id = $u->company_id;
		$local_id = $u->local_id;
		$disabled_local =  (empty($u->local_id)) ? 'disabled' : '';
		
		if(empty($company_id) || empty($local_id) ) {
			$module = (empty($company_id)) ? 'Empresa' : 'Local';
			$module_link = (empty($company_id)) ? 'company/addCompany' : 'local/addLocal';
			if($u->user_type == $this->config->item('IND_ADMIN')) {
				$title = 'Cree ' . $module . ' primero';
				$subtitle = 'Cree ' . $module . ' primero';
				$bg_color = 'red';
				$link = createLink(base_url() . $module_link, ' ', 'icon-ban', 'Crear ' . $module, true);
				$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'HOME Cree una empresa primero', 'BASE_URL' => base_url()];
				$data['BODY'] = $this->parser->parse('notice', ['BASE_URL' => base_url()
						,'BODY_TITLE'			=> $title
						,'BODY_SUBTITLE'		=> $subtitle
						,'BODY_MENU'			=> ''
						,'BG_COLOR'				=> $bg_color
						,'NOTICE'				=> $link
				], TRUE);
			} else {
				$title = 'Configure su lugar de trabajo primero.';
				$subtitle = 'Seleccione empresa y local.';
				$bg_color = 'red';
				$link = '';
				$select_company_id = ['' => 'Seleccione'];
				$select_local_id = [];
				if($u->user_type == $this->config->item('IND_ADMIN')) {
					$c = $this->company->getCompany(['user_id' => $this->UID, 'company_status' => 1, 'user_type' => $this->config->item('IND_ADMIN')]);
					foreach($c as $k) {
						$select_company_id[$k->company_id] = $k->company_social;
					}
				} else {
					$select_company_id[$u->company_id] = $this->gen->getRow('jyc_company', ['company_id' => $u->company_id])->company_social;
				}
				$select_local_id = ['' => 'Seleccione'];
					
					
				if(!empty($company_id)) {
					$local = $this->gen->getData('jyc_local', ['company_id' => $company_id, 'local_status' => 1]);
					foreach($local as $l) {
						$select_local_id[$l->local_id] = $l->local_name;
						$disabled_local = '';
					}
				}
				$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'HOME Cree una empresa primero', 'BASE_URL' => base_url()];
				$data['BODY'] = $this->parser->parse('home_setHome', ['BASE_URL' => base_url()
						,'BODY_TITLE'			=> $title
						,'BODY_SUBTITLE'		=> $subtitle
						,'BODY_MENU'			=> ''
						,'BG_COLOR'				=> $bg_color
						,'URL_POST'				=> base_url() .'home/index/setHome/'
						,'SELECT_LOCAL_ID'		=> form_dropdown(array('name' => 'select_local_id', 'id' => 'select_local_id'), $select_local_id, $local_id, 'class="input-block-level" ' . $disabled_local . ' required')
						,'SELECT_COMPANY_ID'	=> form_dropdown(array('name' => 'select_company_id', 'id' => 'select_company_id'), $select_company_id, $company_id, 'class="input-block-level" required')
						,'BUTTON_SUBMIT'		=> createSubmitButton('Guardar', 'btn-success', 'icon-save')
				], TRUE);
				$data['FOOTER_EXTRA'] = "
				<script>
					$('#select_company_id').change(function() {
						ID = $(this).val();
						if(ID >= 1) {
							$.ajax({
							  url: '" . base_url() . "space/getLocalOptions/' + ID,
							  dataType: 'html'
							}).done(function(row){
								$('#select_local_id').html(row).attr('disabled', false);
							  	$('#select_local_id').html(row).attr('required', true);
							});
						} else {
							$('#select_local_id').html('').attr('disabled', true);
						}
			
					});
					</script>
			";
			}
			
			
			renderPage($data);
			
		} else {
			$select_company_id = ['' => 'Seleccione'];
			$select_local_id = [];
			if($u->user_type == $this->config->item('IND_ADMIN')) {
				$c = $this->company->getCompany(['user_id' => $this->UID, 'company_status' => 1, 'user_type' => $this->config->item('IND_ADMIN')]);
				foreach($c as $k) {
					$select_company_id[$k->company_id] = $k->company_social;
				}
			} else {
				$select_company_id[$u->company_id] = $this->gen->getRow('jyc_company', ['company_id' => $u->company_id])->company_social;
			}
			$select_local_id = ['' => 'Seleccione'];
			
			
			if(!empty($company_id)) {
				$local = $this->gen->getData('jyc_local', ['company_id' => $company_id, 'local_status' => 1]);
				foreach($local as $l) {
					$select_local_id[$l->local_id] = $l->local_name;
				}
			}
			#START CITAS DEL DIA
			$quote_subtitle = 'Citas del D&iacute;a';
			$list_today_events = array();
			$where = ['quote_fec_start >=' => date('Y-m-d H:i:s')
					,'quote_fec_end <=' => date('Y-m-d') . ' 23:59:00'	
					,'user_type'	=> $this->config->item('IND_EMPLOYEE')
			];
			if($this->UTYPE != $this->config->item('IND_ADMIN')) {
				$where['user_id']	= $this->session->userdata('USER')->user_id;
			}
			$events = $this->calendar->getEventsParticipant($company_id, $local_id, $where);
			foreach($events as $ev) {
				$img_participant = (file_exists($this->config->item('FOLDER_UPLOAD') . 'users/' . $ev->user_id) ) ? base_url() . 'assets/uploads/users/' . $ev->user_id . '_thumb' : base_url() . 'img/avatar.png';
				$fs = explode(' ', $ev->quote_fec_start);
				$fe = explode(' ', $ev->quote_fec_end);
				$customer_id = $this->gen->getRow('rel_quote_participant', ['quote_id' => $ev->quote_id, 'user_type' => $this->config->item('IND_CUSTOMER')])->user_id;
				$_c = $this->gen->getRow('gen_user', ['user_id' => $customer_id, 'user_type' => $this->config->item('IND_CUSTOMER')]);
				$customer_name = $_c->user_firstname . ' ' . $_c->user_lastname;
				$list_today_events[] = [
						'PARTICIPANT_ID'	=> $ev->user_id
						,'IMG_PARTICIPANT'	=> $img_participant
						,'LINK_PARTICIPANT'	=> base_url() . 'employee/viewEmployee/' . $ev->user_id
						,'QUOTE_TITLE'		=> $ev->quote_title
						,'QUOTE_FEC_START'	=> $fs[1]
						,'QUOTE_FEC_END'	=> $fe[1]
						,'LINK_QUOTE'		=> base_url() . 'calendar/viewEvent/' . $ev->quote_id
						,'CUSTOMER_NAME'	=> $customer_name
						,'CUSTOMER_LINK'	=> base_url() . 'customer/viewCustomer/' . $customer_id
				];
			}
			#END CITAS DEL DIA
			$data['HEADER'] = ['PAGE_TITLE_HEADER' => 'HOME ', 'BASE_URL' => base_url()];
			$data['BODY'] = $this->parser->parse('home/home_' . $this->UTYPE, ['BASE_URL' => base_url()
					,'BODY_TITLE'			=> 'Seleccionar lugar de trabajo'
					,'BODY_SUBTITLE'		=> $subtitle
					,'BODY_MENU'			=> ''
					,'BG_COLOR'				=> $bg_color
					,'URL_POST'				=> base_url() .'home/index/setHome/'
					,'QUOTE_SUBTITLE'		=> $quote_subtitle
					,'TOTAL_QUOTE'			=> count($list_today_events)
					,'INPUT_QUOTE_TITLE'	=> form_input(array('name' => 'input_quote_title', 'id' => 'input_quote_title'), '', 'class="span12" required')
					,'INPUT_CUSTOMER_NAME'	=> form_input(array('name' => 'input_customer_name', 'id' => 'input_customer_name'), '', 'class="span12" required')
					,'INPUT_CUSTOMER_ID'	=> form_hidden('input_customer_id', '', 'id="input_customer_id"')
					,'INPUT_EMPLOYEE_NAME'	=> form_input(array('name' => 'input_employee_name', 'id' => 'input_employee_name'), '', 'class="span12" required')
					,'INPUT_EMPLOYEE_ID'	=> form_hidden('input_employee_id', '', 'id="input_employee_id"')
					,'SELECT_LOCAL_ID'		=> form_dropdown(array('name' => 'select_local_id', 'id' => 'select_local_id'), $select_local_id, $local_id, 'class="input-block-level" ' . $disabled_local . ' required')
					,'SELECT_COMPANY_ID'	=> form_dropdown(array('name' => 'select_company_id', 'id' => 'select_company_id'), $select_company_id, $company_id, 'class="input-block-level" required')
					,'BUTTON_SUBMIT'		=> createSubmitButton('Guardar', 'btn-success', 'icon-save')
					,'LIST_TODAY_EVENTS'	=> $list_today_events
			], TRUE);
			$data['FOOTER_EXTRA'] = "
				<script>
				$('#input_customer_name').autocomplete({
				    serviceUrl: '" . base_url() . "customer/searchCustomer/" . $this->session->userdata('USER')->company_id . "/',
				    minChars: 3,
				    onSelect: function (suggestion) {
				        $('input[name=\"input_customer_id\"]').val(suggestion.user_id);
				    }
				});
				$('#input_employee_name').autocomplete({
				    serviceUrl: '" . base_url() . "employee/searchEmployee/" . $this->session->userdata('USER')->company_id . "/',
				    minChars: 3,
				    onSelect: function (suggestion) {
				        $('input[name=\"input_employee_id\"]').val(suggestion.user_id);
				    }
				});
				$('#calendar').fullCalendar({
					header: {
			            left: 'prev,next today',
			            center: 'title',
			            right: 'month,agendaWeek,agendaDay'
			        }
				    ,defaultView: 'agendaWeek'
				    ,editable: false
        			,droppable: false
				    ,selectable: true
			        ,selectHelper: true
				    ,select: function(start, end, allDay, view) {
				    	//console.log(start + '--' + end + '--' + allDay);
				    	titulo = $('#input_quote_title').val();
				    	customer_id = $('input[name=\"input_customer_id\"]').val();
				    	employee_id = $('input[name=\"input_employee_id\"]').val();
				    	if(customer_id.length <= 0 || employee_id.length <= 0 || titulo.length <= 0 ) {
				    		return false;
				    	}
						$.post( '".base_url()."calendar/createFastEvent/', { inicio: start
								,final: end
								,fullday: allDay
								,titulo: titulo
								,customer_id: customer_id
								,employee_id: employee_id
						}).done(function( data ) {
							$('#calendar').fullCalendar('refetchEvents');
						});
				    }
				    ,events: {
				        url: '" . base_url() . "calendar/getEvents/" . $this->session->userdata('USER')->company_id . "/" . $this->session->userdata('USER')->local_id . "/',
				        type: 'POST',
				        data: {
				            company_id: '" . $this->session->userdata('USER')->company_id . "',
				            local_id: '" . $this->session->userdata('USER')->local_id . "'
				        },
				        error: function() {
				            alert('there was an error while fetching events!');
				        },
				        color: 'yellow',
				        textColor: 'black'
				    }
				});
				$('#select_company_id').change(function() {
					ID = $(this).val();
					if(ID >= 1) {
						$.ajax({
						  url: '" . base_url() . "space/getLocalOptions/' + ID,
						  dataType: 'html'
						}).done(function(row){
							$('#select_local_id').html(row).attr('disabled', false);
						  	$('#select_local_id').html(row).attr('required', true);
						});
					} else {
						$('#select_local_id').html('').attr('disabled', true);
					}
		
				});
				</script>
		";
			renderPage($data);
		}
	}
}
