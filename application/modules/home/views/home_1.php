<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
			<div class="row-fluid">
                
                <div class="span8">
	                <div class="row-fluid">
		                <div class="span12">
		                	<div class="widget yellow">
		                        <div class="widget-title">
		                        	<h4><i class="icon-calendar"></i> T&iacute;tulo</h4>
		                        </div>
		                        <div class="control-group">
		                        	<div class="controls controls-row">
		                        		{INPUT_QUOTE_TITLE}
		                        	</div>
		                        </div>
							</div>
		                </div>
	                </div>
	                <div class="row-fluid">
		                <div class="span6">
		                	<div class="widget yellow">
		                        <div class="widget-title">
		                        	<h4><i class="icon-calendar"></i> Cliente</h4>
		                        </div>
		                        <div class="control-group">
		                        	<div class="controls controls-row">
		                        		{INPUT_CUSTOMER_ID}
		                        		{INPUT_CUSTOMER_NAME}
		                        	</div>
		                        </div>
							</div>
		                </div>
		                <div class="span6">
		                	<div class="widget yellow">
		                        <div class="widget-title">
		                        	<h4><i class="icon-calendar"></i> Trabajador</h4>
		                        </div>
		                        <div class="control-group">
		                        	<div class="controls controls-row">
		                        		{INPUT_EMPLOYEE_ID}
		                        		{INPUT_EMPLOYEE_NAME}
		                        	</div>
		                        </div>
							</div>
		                </div>
	                </div>
	                <div class="row-fluid">
		                <div class="span12 responsive" data-tablet="span12 fix-margin" data-desktop="span12">
		                    <!-- BEGIN CALENDAR PORTLET-->
		                    <div class="widget yellow">
		                        <div class="widget-title">
		                            <h4><i class="icon-calendar"></i> Calendario</h4>
		                        </div>
		                        <div class="widget-body">
		                            <div id="calendar" class="has-toolbar"></div>
		                        </div>
		                    </div>
		                    <!-- END CALENDAR PORTLET-->
		                </div>
	                </div>
	                <div class="clearfix"></div>
	                
                </div>
                
                
                
                <div class="span4">
                    <!-- BEGIN VALIDATION STATES-->
                    <div class="widget {BG_COLOR}">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> {BODY_SUBTITLE}</h4>
                        </div>
                        <div class="widget-body form cmxform">
							<form class=" form-vertical" id="signupForm" method="post" action="{URL_POST}">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="control-group">
                                            <label class="control-label" >EMPRESA</label>
                                            <div class="controls controls-row">
                                                {SELECT_COMPANY_ID}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="control-group">
                                            <label class="control-label" >LOCAL</label>
                                            <div class="controls controls-row">
                                                {SELECT_LOCAL_ID}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-actions">
                                    {BUTTON_SUBMIT}
                                </div>
							</form>
						</div>
					</div>
					
					<div class="widget blue">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> {QUOTE_SUBTITLE}</h4>
                            <span class="badge badge-important">{TOTAL_QUOTE}</span>
                        </div>
                        <div class="widget-body">
                        	<div class="timeline-messages">
                        		{LIST_TODAY_EVENTS}
                        		<div class="msg-time-chat">
                                     <a class="message-img" href="{LINK_PARTICIPANT}"><img alt="" src="{IMG_PARTICIPANT}" class="avatar"></a>
                                     <div class="message-body msg-in">
                                         <span class="arrow"></span>
                                         <div class="text">
                                             <p class="attribution">Cliente: <a href="{CUSTOMER_LINK}">{CUSTOMER_NAME}</a> <p>
                                             <p>Hora: desde {QUOTE_FEC_START} hasta {QUOTE_FEC_END}</p>
                                             <p>T&iacute;tulo: <a href="{LINK_QUOTE}">{QUOTE_TITLE}</a></p>
                                         </div>
                                     </div>
                                 </div>
                                 {/LIST_TODAY_EVENTS}
                        	</div>
                        </div>
					</div>
					
					
				</div>
			</div>