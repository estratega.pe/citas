<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
			<div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN VALIDATION STATES-->
                    <div class="widget {BG_COLOR}">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> {BODY_SUBTITLE}</h4>
                        </div>
                        <div class="widget-body form cmxform">
							<form class=" form-vertical" id="signupForm" method="post" action="{URL_POST}">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="control-group">
                                            <label class="control-label" >EMPRESA</label>
                                            <div class="controls controls-row">
                                                {SELECT_COMPANY_ID}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="control-group">
                                            <label class="control-label" >LOCAL</label>
                                            <div class="controls controls-row">
                                                {SELECT_LOCAL_ID}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-actions">
                                    {BUTTON_SUBMIT}
                                </div>
							</form>
						</div>
					</div>					
				</div>
			</div>