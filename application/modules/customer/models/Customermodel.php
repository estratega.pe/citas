<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class customerModel extends MX_Controller {
	
	const LOCAL = 'jyc_local';
	const EMPRESA = 'jyc_company';
	const SELECT = 'gen_select';
	const CUSTOMER = 'gen_user';
	const R_C_U = 'rel_company_user';
	const R_C_C = 'rel_company_customer';
	
	function __construct()
	{
		parent::__construct();
	}
	
	function getCustomer($where = null, $like = null, $limit = null, $start = null, $or_like = null) {
		$this->db->select(self::R_C_C . '.*,'
				. self::EMPRESA .'.company_social, '
				. self::EMPRESA .'.company_status, '
				. self::CUSTOMER . '.*
    	');
		$this->db->from(self::R_C_C);
		$this->db->join(self::EMPRESA, self::EMPRESA. '.company_id = ' . self::R_C_C . '.company_id');
		$this->db->join(self::CUSTOMER, self::CUSTOMER . '.user_id = ' . self::R_C_C . '.user_id');
		$this->db->join(self::R_C_U, self::R_C_U . '.company_id = ' . self::R_C_C . '.company_id');
		$this->db->where(self::R_C_C . '.user_type', $this->config->item('IND_CUSTOMER'));
		$this->db->where(self::R_C_U . '.user_type', $this->config->item('IND_ADMIN'));
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like)) {
			$this->db->group_start();
			$this->db->like($like);
			if(is_null($or_like) AND !is_array($or_like))
				$this->db->group_end();
		}
		if(!is_null($or_like) AND is_array($or_like)) {
			if(is_null($like) AND !is_array($like))
				$this->db->group_start();
			$this->db->or_like($or_like);
			$this->db->group_end();
		}
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
		return $this->db->get()->result();
	}
	
	function getTotalCustomer($where = null, $like = null, $limit = null, $start = null, $or_like = null) {
		$this->db->select(self::R_C_C . '.*,'
				. self::EMPRESA .'.company_social, '
				. self::EMPRESA .'.company_status, '
				. self::CUSTOMER . '.*
    	');
		$this->db->from(self::R_C_C);
		$this->db->join(self::EMPRESA, self::EMPRESA. '.company_id = ' . self::R_C_C . '.company_id');
		$this->db->join(self::CUSTOMER, self::CUSTOMER . '.user_id = ' . self::R_C_C . '.user_id');
		$this->db->join(self::R_C_U, self::R_C_U . '.company_id = ' . self::R_C_C . '.company_id');
		$this->db->where(self::R_C_C . '.user_type', $this->config->item('IND_CUSTOMER'));
		$this->db->where(self::R_C_U . '.user_type', $this->config->item('IND_ADMIN'));
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like)) {
			$this->db->group_start();
			$this->db->like($like);
			if(is_null($or_like) AND !is_array($or_like))
				$this->db->group_end();
		}
		if(!is_null($or_like) AND is_array($or_like)) {
			if(is_null($like) AND !is_array($like))
				$this->db->group_start();
			$this->db->or_like($or_like);
			$this->db->group_end();
		}
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
		return $this->db->count_all_results();
	}
	
	function getRowCustomer($where = null) {
		$this->db->select(self::R_C_C . '.*,'
				. self::EMPRESA .'.company_social, '
				. self::EMPRESA .'.company_status, '
				. self::CUSTOMER . '.*
    	');
		$this->db->from(self::R_C_C);
		$this->db->join(self::EMPRESA, self::EMPRESA. '.company_id = ' . self::R_C_C . '.company_id');
		$this->db->join(self::CUSTOMER, self::CUSTOMER . '.user_id = ' . self::R_C_C . '.user_id');
		$this->db->join(self::R_C_U, self::R_C_U . '.company_id = ' . self::R_C_C . '.company_id');
		$this->db->where(self::R_C_C . '.user_type', $this->config->item('IND_CUSTOMER'));
		$this->db->where(self::R_C_U . '.user_type', $this->config->item('IND_ADMIN'));
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		return $this->db->get_where()->row();
	}
	
	function saveCustomer($data, $dataRel) {
		$this->db->trans_start();
		$this->db->insert(self::CUSTOMER, $data);
		$ID = $this->db->insert_id();
		$dataRel['user_id'] = $ID;
		$this->db->insert(self::R_C_C, $dataRel);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return $ID;
	}
	
	function updateCustomer($data, $where, $_data, $_where) {
		$this->db->trans_start();
		$this->db->update(self::CUSTOMER, $data, $where);
		$this->db->update(self::R_C_C, $_data, $_where);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function setCustomer($data, $where, $_where) {
		$this->db->trans_start();
		$this->db->update(self::CUSTOMER, $data, $where);
		$this->db->update(self::R_C_C, $data, $_where);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function searchCustomerBACK($search, $field = 'email') {
		$this->db->from(self::CUSTOMER);
		$this->db->where('user_type', $this->config->item('IND_CUSTOMER'));
		$this->db->like('user_' . $field, $search);
		$this->db->limit(20);
		return $this->db->get()->result();
	}
	
	function searchCustomer($search, $company) {
		$this->db->from(self::CUSTOMER);
		$this->db->join(self::R_C_C, self::R_C_C . '.user_id=' . self::CUSTOMER . '.user_id');
		$this->db->where(self::R_C_C . '.user_type', $this->config->item('IND_CUSTOMER'));
		$this->db->where(self::R_C_C . '.company_id', $company);
		$this->db->like('user_firstname', $search);
		$this->db->or_like('user_lastname', $search);
		$this->db->limit(20);
		return $this->db->get()->result();
	}
}
