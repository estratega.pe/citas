<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
			<div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN VALIDATION STATES-->
                    <div class="widget {BG_COLOR}">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> {BODY_SUBTITLE}</h4>
                        </div>
                        <div class="widget-body form">
                            <!-- BEGIN FORM-->
                            <form class="cmxform form-horizontal" id="signupForm" method="post" action="{URL_POST}">
                                <div class="control-group ">
                                    <label for="lastname" class="control-label">EMPRESA</label>
                                    <div class="controls">
                                        {SELECT_COMPANY_ID}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="lastname" class="control-label">Nombres</label>
                                    <div class="controls">
                                        {INPUT_USER_FIRSTNAME}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="firstname" class="control-label">Apellidos</label>
                                    <div class="controls">
                                        {INPUT_USER_LASTNAME}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="firstname" class="control-label">E-mail</label>
                                    <div class="controls">
                                        {INPUT_USER_EMAIL}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="firstname" class="control-label">Sexo</label>
                                    <div class="controls">
                                        {SELECT_USER_SEX}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="lastname" class="control-label">Tipo Doc.</label>
                                    <div class="controls">
                                        {SELECT_USER_DOC_TYPE}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="firstname" class="control-label">N&uacute;mero Doc.</label>
                                    <div class="controls">
                                        {INPUT_USER_DOC_NUMBER}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="lastname" class="control-label">Cliente Desde</label>
                                    <div class="controls">
                                    	<div class="input-append date" id="dpYears" data-date="11-11-2016"
                                             data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                                            {INPUT_USER_FEC_START_T}
                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="lastname" class="control-label">Fecha de Nacimiento</label>
                                    <div class="controls">
                                    	<div class="input-append date" id="dpNac" data-date="11-11-2016"
                                             data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                                            {INPUT_USER_FEC_NAC}
                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    {BUTTON_SUBMIT}
                                    {BUTTON_CANCEL}
                                </div>

                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END VALIDATION STATES-->
                </div>
			</div>