<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class localModel extends MX_Controller {
	
	const LOCAL = 'jyc_local';
	const EMPRESA = 'jyc_company';
	const R_P_C = 'rel_company_user';
	
    function __construct()
    {
        parent::__construct();
        $this->load->model('company/companyModel', 'company');
    }
    
    function getLocal($where = null, $like = null, $limit = null, $start = null) {
    	$this->db->select('*');
    	$this->db->from(self::LOCAL);
    	$this->db->join(self::EMPRESA, self::EMPRESA . '.company_id=' . self::LOCAL . '.company_id');
    	$this->db->join(self::R_P_C, self::R_P_C . '.company_id=' . self::EMPRESA . '.company_id');
    	if(!is_null($where) AND is_array($where))
    		$this->db->where($where);
    	if(!is_null($like) AND is_array($like))
    		$this->db->like($like);
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($limit, $start);
    	return $this->db->get()->result();
    }
	
	function getTotalLocal($where = null, $like = null, $start = null, $limit = null) {
		$this->db->select('*');
		$this->db->from(self::LOCAL);
		$this->db->join(self::EMPRESA, self::EMPRESA . '.company_id=' . self::LOCAL . '.company_id');
		$this->db->join(self::R_P_C, self::R_P_C . '.company_id=' . self::EMPRESA . '.company_id');
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($start, $limit);
		return $this->db->count_all_results();
	}
	
	function getRowLocal($where) {
		$this->db->select('*');
		$this->db->from(self::LOCAL);
		$this->db->join(self::EMPRESA, self::EMPRESA . '.company_id=' . self::LOCAL . '.company_id');
		$this->db->join(self::R_P_C, self::R_P_C . '.company_id=' . self::EMPRESA . '.company_id');
		$this->db->where($where);
		return $this->db->get()->row();
	}
	
	
	
	
	
	
	
	function _getRowLocal($where = null) {
		$this->db->select(self::LOCAL . '.*, '
				. self::EMPRESA . '.ind_status as ' . self::EMPRESA . '_ind_status, '
				. self::EMPRESA . '.des_social as ' . self::EMPRESA . '_des_social
    	');
		$this->db->from(self::LOCAL);
		$this->db->join(self::EMPRESA, self::EMPRESA . '.company_id = ' . self::LOCAL . '.company_id');
		$this->db->join(self::R_P_C, self::R_P_C . '.company_id = ' . self::LOCAL . '.company_id');
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		return $this->db->get_where()->row();
	}
}
