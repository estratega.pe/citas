<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
			<div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN VALIDATION STATES-->
                    <div class="widget {BG_COLOR}">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> {BODY_SUBTITLE}</h4>
                        </div>
                        <div class="widget-body form cmxform form-horizontal">
                                <div class="control-group ">
                                    <label for="firstname" class="control-label">Raz&oacute;n Social</label>
                                    <div class="controls">
                                        {INPUT_DES_SOCIAL}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="lastname" class="control-label">Nombre Comercial</label>
                                    <div class="controls">
                                        {INPUT_DES_COMERCIAL}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="username" class="control-label">R.U.C</label>
                                    <div class="controls">
                                        {INPUT_DES_RUC}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="username" class="control-label">E-mail</label>
                                    <div class="controls">
                                        {INPUT_COMPANY_EMAIL}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="username" class="control-label">Tel&eacute;fono</label>
                                    <div class="controls">
                                        {INPUT_COMPANY_PHONE}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="password" class="control-label">Direcci&oacute;n</label>
                                    <div class="controls">
                                        {TEXTAREA_DES_DIRECCION}
                                    </div>
                                </div>

                                <div class="form-actions">
                                    {LINK_BACK}
                                </div>
                        </div>
                    </div>
                    <!-- END VALIDATION STATES-->
                </div>
			</div>