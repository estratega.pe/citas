<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
			<div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN VALIDATION STATES-->
                    <div class="widget {BG_COLOR}">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> {BODY_SUBTITLE}</h4>
                        </div>
                        <div class="widget-body form cmxform form-horizontal">
                                <div class="control-group ">
                                    <label for="lastname" class="control-label">Empresa</label>
                                    <div class="controls">
                                        {SELECT_COMPANY_ID}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="lastname" class="control-label">Local</label>
                                    <div class="controls">
                                        {SELECT_LOCAL_ID}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="lastname" class="control-label">Espacio</label>
                                    <div class="controls">
                                        {SELECT_SPACE_ID}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="customer" class="control-label">Cliente(*)</label>
                                    <div class="controls">
                                        {INPUT_CUSTOMER_ID}
                                        {INPUT_CUSTOMER_NAME}
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <label for="customer" class="control-label">Trabajador(*)</label>
                                    <div class="controls">
                                        {INPUT_EMPLOYEE_ID}
                                        {INPUT_EMPLOYEE_NAME}
                                    </div>
                                </div>
                                
                                <div class="control-group ">
                                    <label for="lastname" class="control-label">Dia Cita(*)</label>
                                    <div class="controls">
                                        {INPUT_FEC_START} 
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                    <label class="control-label">Hora Inicio(*)</label>

                                    <div class="controls">
                                        {INPUT_HOUR_START}
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Hora Fin(*)</label>

                                    <div class="controls">
                                    	{INPUT_HOUR_END}
                                    </div>
                                </div>
                                
                                <div class="control-group ">
                                    <label for="lastname" class="control-label">T&iacute;tulo(*)</label>
                                    <div class="controls">
                                        {INPUT_QUOTE_TITLE}
                                    </div>
                                </div>
                                
                                <div class="control-group ">
                                    <label for="password" class="control-label">Descripci&oacute;n</label>
                                    <div class="controls">
                                        {TEXTAREA_DES_DESCRIPTION}
                                    </div>
                                </div>
                                
                                <div class="form-actions">
                                    {LINK_BACK}
                                    {INPUT_DELETE_EVENT}
                                </div>
                        </div>
                    </div>
                    <!-- END VALIDATION STATES-->
                </div>
			</div>