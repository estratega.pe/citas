<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class calendarModel extends MX_Controller {
	
	const USER = 'gen_user';
	const CALENDAR = 'jyc_quote';
	const COMPANY = 'jyc_company';
	const LOCAL = 'jyc_local';
	const R_C_E = 'rel_company_employee';
	const R_C_U = 'rel_company_user';
	const R_Q_P = 'rel_quote_participant';
	
    function __construct()
    {
        parent::__construct();
    }
    
    function getEvents($company_id, $local_id, $where) {
    	$this->db->select('*');
    	$this->db->from(self::CALENDAR);
    	$this->db->where('company_id', $company_id);
    	$this->db->where('local_id', $local_id);
    	$this->db->where($where);
    	return$this->db->get()->result();
    }
    
    function getEventsParticipant($company_id, $local_id, $where) {
    	$this->db->select('*');
    	$this->db->from(self::CALENDAR);
    	$this->db->join(self::R_Q_P, self::R_Q_P . '.quote_id=' . self::CALENDAR . '.quote_id');
    	$this->db->where('company_id', $company_id);
    	$this->db->where('local_id', $local_id);
    	$this->db->where($where);
    	$this->db->order_by('quote_fec_start', 'ASC');
    	return$this->db->get()->result();
    }
    
    function getRowEvent($event) {
    	$this->db->select('*');
    	$this->db->from(self::CALENDAR);
    	$this->db->join(self::COMPANY, self::COMPANY . '.company_id=' . self::CALENDAR . '.company_id');
    	$this->db->join(self::LOCAL, self::LOCAL . '.local_id=' . self::CALENDAR . '.local_id');
    	if($this->session->userdata('USER')->user_type == $this->config->item('IND_ADMIN')) {
    		$this->db->join(self::R_C_U, self::R_C_U . '.company_id=' . self::CALENDAR . '.company_id');
    		$this->db->where([self::R_C_U . '.user_id' => $this->session->userdata('USER')->user_id
    				,self::R_C_U . '.user_type'	=> $this->config->item('IND_ADMIN')
    		]);
    	} else {
    		$this->db->join(self::R_C_E, self::R_C_E . '.company_id=' . self::CALENDAR . '.company_id');
    		$this->db->where([self::R_C_E . '.user_id' => $this->session->userdata('USER')->user_id
    				,self::R_C_E . '.user_type'	=> $this->config->item('IND_EMPLOYEE')
    		]);
    	}
    	$this->db->where(self::CALENDAR . '.quote_id', $event);
    	return $this->db->get()->row();
    }
    
    function deleteEvent($ID) {
    	$this->db->trans_start();
    	$this->db->delete(self::R_Q_P, ['quote_id' => $ID]);
    	$this->db->delete(self::CALENDAR, ['quote_id' => $ID]);
    	$this->db->trans_complete();
    	if ($this->db->trans_status() === FALSE)
    		return FALSE;
    	else
    		return TRUE;
    }
    
    function getQuoteCustomer($where = null, $like = null, $limit = null, $start = null) {
    	$this->db->select(self::CALENDAR . '.*, '
    			. self::USER . '.*, '
    			. self::R_Q_P . '.quote_id as ' . self::R_Q_P . '_quote_id, '
    			. self::R_Q_P . '.user_id as ' . self::R_Q_P . '_user_id, '
    			. self::R_Q_P . '.user_type as ' . self::R_Q_P . '_user_type, '
    			. self::COMPANY . '.company_id as ' . self::COMPANY . '_company_id, '
    			. self::COMPANY . '.company_social as ' . self::COMPANY . '_company_social, '
    			. self::R_C_U . '.company_id as ' . self::R_C_U . '_company_id, '
    			. self::R_C_U . '.user_id as ' . self::R_C_U . '_user_id, '
    			. self::R_C_U . '.user_type as ' . self::R_C_U . '_user_type 
    	');
    	$this->db->from(self::CALENDAR);
    	$this->db->join(self::R_Q_P, self::R_Q_P . '.quote_id=' . self::CALENDAR . '.quote_id');
    	$this->db->join(self::USER, self::USER . '.user_id=' . self::R_Q_P . '.user_id');
    	$this->db->join(self::COMPANY, self::COMPANY . '.company_id=' . self::CALENDAR . '.company_id');
    	$this->db->join(self::R_C_U, self::R_C_U . '.company_id=' . self::COMPANY . '.company_id');
    	$this->db->where($where);
    	if(!is_null($like) AND is_array($like))
    		$this->db->like($like);
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($limit, $start);
    	$this->db->order_by('quote_fec_start', 'DESC');
    	return$this->db->get()->result();
    }
    
    function getTotalQuoteCustomer($where = null, $like = null, $limit = null, $start = null) {
    	$this->db->select(self::CALENDAR . '.*, '
    			. self::R_Q_P . '.quote_id as ' . self::R_Q_P . '_quote_id, '
    			. self::R_Q_P . '.user_id as ' . self::R_Q_P . '_user_id, '
    			. self::R_Q_P . '.user_type as ' . self::R_Q_P . '_user_type, '
    			. self::COMPANY . '.company_id as ' . self::COMPANY . '_company_id, '
    			. self::COMPANY . '.company_social as ' . self::COMPANY . '_company_social, '
    			. self::R_C_U . '.company_id as ' . self::R_C_U . '_company_id, '
    			. self::R_C_U . '.user_id as ' . self::R_C_U . '_user_id, '
    			. self::R_C_U . '.user_type as ' . self::R_C_U . '_user_type
    	');
    	$this->db->from(self::CALENDAR);
    	$this->db->join(self::R_Q_P, self::R_Q_P . '.quote_id=' . self::CALENDAR . '.quote_id');
    	$this->db->join(self::USER, self::USER . '.user_id=' . self::R_Q_P . '.user_id');
    	$this->db->join(self::COMPANY, self::COMPANY . '.company_id=' . self::CALENDAR . '.company_id');
    	$this->db->join(self::R_C_U, self::R_C_U . '.company_id=' . self::COMPANY . '.company_id');
    	$this->db->where($where);
    	if(!is_null($like) AND is_array($like))
    		$this->db->like($like);
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($limit, $start);
    	$this->db->order_by('quote_fec_start', 'DESC');
    	return $this->db->count_all_results();
    }
}
