<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends MX_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('home/homeModel', 'home');
		$this->load->model('company/companyModel', 'company');
		$this->load->model('calendar/calendarModel', 'calendar');
		
	}
	
	public function index()
	{
		redirect(base_url());
		exit();
	}
	
	#
	public function cronAdminDaylyEvent() {
		$this->db->from('rel_company_user');
		$this->db->join('gen_user', 'gen_user.user_id=rel_company_user.user_id');
		$this->db->group_by('rel_company_user.user_id');
		$admins = $this->db->get()->result();
		foreach($admins as $adm) {
			$msg = 'N';
			$admin_content = '';
			//$content_admin .= "&ensp;ADMIN: &thinsp; $adm->user_login <br />\n";
			$admin_email = $adm->user_email;
			$this->db->from('rel_company_user');
			$this->db->join('jyc_company', 'jyc_company.company_id=rel_company_user.company_id');
			$this->db->where('rel_company_user.user_id', $adm->user_id);
			$companies = $this->db->get()->result();
			foreach($companies as $company) {
				$subject_admin = "**Alertas** Citas del d�a en $company->company_social \n";
				$admin_content .= "&ensp;&emsp;EMPRESA: &thinsp; $company->company_social <br />\n";
				$this->db->from('jyc_local');
				$this->db->where('company_id', $company->company_id);
				$locales = $this->db->get()->result();
				foreach($locales as $local) {
					//$admin_content .= "&ensp;&emsp;&emsp;LOCAL: &thinsp; $local->local_name <br />\n";
					$list_today_events = array();
					$where = ['quote_fec_start >='	=> date('Y-m-d') . '00:00:00'
							,'quote_fec_end <='		=> date('Y-m-d') . ' 23:59:00'
							,'user_type'			=> $this->config->item('IND_EMPLOYEE')
					];
					$events = $this->calendar->getEventsParticipant($company->company_id, $local->local_id, $where);
					if(count($events)>= 0x0001) {
						$admin_content .= "<br />\n== CITAS EN EL LOCAL $local->local_name ==<br />\n";
					}
					foreach($events as $event) {
						$cust = $this->db->get_where('rel_quote_participant', ['quote_id' => $event->quote_id, 'user_type' => $this->config->item('IND_CUSTOMER')])->row();
						$customer = $this->db->get_where('gen_user', ['user_id' => $cust->user_id])->row();
						$cliente = $customer->user_firstname . ' ' . $customer->user_lastname;
						
						$empl = $this->db->get_where('rel_quote_participant', ['quote_id' => $event->quote_id, 'user_type' => $this->config->item('IND_EMPLOYEE')])->row();
						$employee = $this->db->get_where('gen_user', ['user_id' => $empl->user_id])->row();
						$trabajador = $employee->user_firstname . ' ' . $employee->user_lastname;
						
						$fs = explode(" ", $event->quote_fec_start);
						$fe = explode(" ", $event->quote_fec_end);
						$admin_content .= "******************************<br />\n";
						$admin_content .= "*&ensp;T&Iacute;TULO: &thinsp; $event->quote_title <br />\n";
						$admin_content .= "*&ensp;HORA INICIO: &thinsp; " . converDate($fs[0]) ." $fs[1]<br />\n";
						$admin_content .= "*&ensp;HORA FIN: &thinsp; " . converDate($fe[0]) ." $fe[1]<br />\n";
						$admin_content .= "*&ensp;ATENCION AL CLIENTE: &thinsp; " . $cliente . "<br />\n";
						$admin_content .= "*&ensp;TRABAJADOR: &thinsp; $trabajador <br />\n";
					}
					if(count($events)>= 0x0001) {
						$admin_content .= "******************************<br /><br />";
						$msg = 'Y';
					}
				}
			}
			$admin_content .= "<br />";
			if($msg == 'Y') {
				$this->_sendEmail($admin_email, $this->config->item('EMAIL_USER'), $subject_admin, $admin_content);
			}
		}
	}
	
	public function cronAdminTomorrowEvent() {
		//$date = date('Y-m-d') . '00:00:00';
		//echo date('Y-m-d H:i:s', strtotime($date . '+1 day'));
		$this->db->from('rel_company_user');
		$this->db->join('gen_user', 'gen_user.user_id=rel_company_user.user_id');
		$this->db->group_by('rel_company_user.user_id');
		$admins = $this->db->get()->result();
		foreach($admins as $adm) {
			$msg = 'N';
			$admin_content = '';
			//$content_admin .= "&ensp;ADMIN: &thinsp; $adm->user_login <br />\n";
			$admin_email = $adm->user_email;
			$this->db->from('rel_company_user');
			$this->db->join('jyc_company', 'jyc_company.company_id=rel_company_user.company_id');
			$this->db->where('rel_company_user.user_id', $adm->user_id);
			$companies = $this->db->get()->result();
			foreach($companies as $company) {
				$subject_admin = "**Alertas** Citas para ma�ana en $company->company_social \n";
				$admin_content .= "&ensp;&emsp;EMPRESA: &thinsp; $company->company_social <br />\n";
				//$admin_content .= "&ensp;&emsp;TRABAJADOR: &thinsp; $adm->user_firstname $adm->user_lastname <br />\n";
				$this->db->from('jyc_local');
				$this->db->where('company_id', $company->company_id);
				$locales = $this->db->get()->result();
				foreach($locales as $local) {
					//$admin_content .= "&ensp;&emsp;&emsp;LOCAL: &thinsp; $local->local_name <br />\n";
					$list_today_events = array();
					$_fs = date('Y-m-d') . '00:00:00';
					$_fe = date('Y-m-d') . ' 23:59:00';
					$where = ['quote_fec_start >='	=> date('Y-m-d H:i:s', strtotime($_fs . '+1 day'))
							,'quote_fec_end <='		=> date('Y-m-d H:i:s', strtotime($_fe . '+1 day'))
							,'user_type'			=> $this->config->item('IND_EMPLOYEE')
					];
					$events = $this->calendar->getEventsParticipant($company->company_id, $local->local_id, $where);
					if(count($events)>= 0x0001) {
						$admin_content .= "<br />\n== CITAS EN EL LOCAL $local->local_name ==<br />\n";
					}
					foreach($events as $event) {
						$cust = $this->db->get_where('rel_quote_participant', ['quote_id' => $event->quote_id, 'user_type' => $this->config->item('IND_CUSTOMER')])->row();
						$customer = $this->db->get_where('gen_user', ['user_id' => $cust->user_id])->row();
						$cliente = $customer->user_firstname . ' ' . $customer->user_lastname;
						
						$empl = $this->db->get_where('rel_quote_participant', ['quote_id' => $event->quote_id, 'user_type' => $this->config->item('IND_EMPLOYEE')])->row();
						$employee = $this->db->get_where('gen_user', ['user_id' => $empl->user_id])->row();
						$trabajador = $employee->user_firstname . ' ' . $employee->user_lastname;
						
						$fs = explode(" ", $event->quote_fec_start);
						$fe = explode(" ", $event->quote_fec_end);
						$admin_content .= "******************************<br />\n";
						$admin_content .= "*&ensp;T&Iacute;TULO: &thinsp; $event->quote_title <br />\n";
						$admin_content .= "*&ensp;HORA INICIO: &thinsp; " . converDate($fs[0]) ." $fs[1]<br />\n";
						$admin_content .= "*&ensp;HORA FIN: &thinsp; " . converDate($fe[0]) ." $fe[1]<br />\n";
						$admin_content .= "*&ensp;ATENCION AL CLIENTE: &thinsp; " . $cliente . "<br />\n";
						$admin_content .= "*&ensp;TRABAJADOR: &thinsp; $trabajador <br />\n";
					}
					if(count($events)>= 0x0001) {
						$admin_content .= "******************************<br /><br />";
						$msg = 'Y';
					}
				}
			}
			$admin_content .= "<br />";
			if($msg == 'Y') {
				$this->_sendEmail($admin_email, $this->config->item('EMAIL_USER'), $subject_admin, $admin_content);
			}
		}
	}
	
	public function cronUserDaylyEvent() {
		$this->db->from('rel_company_employee');
		$this->db->join('gen_user', 'gen_user.user_id=rel_company_employee.user_id');
		$this->db->group_by('rel_company_employee.user_id');
		$admins = $this->db->get()->result();
		foreach($admins as $adm) {
			$msg = 'N';
			$admin_content = '';
			//$content_admin .= "&ensp;ADMIN: &thinsp; $adm->user_login <br />\n";
			$admin_email = $adm->user_email;
			$this->db->from('rel_company_employee');
			$this->db->join('jyc_company', 'jyc_company.company_id=rel_company_employee.company_id');
			$this->db->where('rel_company_employee.user_id', $adm->user_id);
			$companies = $this->db->get()->result();
			foreach($companies as $company) {
				$subject_admin = "**Alertas** Citas del d�a en $company->company_social \n";
				$admin_content .= "&ensp;&emsp;EMPRESA: &thinsp; $company->company_social <br />\n";
				$admin_content .= "&ensp;&emsp;TRABAJADOR: &thinsp; $adm->user_firstname $adm->user_lastname <br />\n";
				$this->db->from('jyc_local');
				$this->db->where('company_id', $company->company_id);
				$locales = $this->db->get()->result();
				foreach($locales as $local) {
					//$admin_content .= "&ensp;&emsp;&emsp;LOCAL: &thinsp; $local->local_name <br />\n";
					$list_today_events = array();
					$where = ['quote_fec_start >='	=> date('Y-m-d') . '00:00:00'
							,'quote_fec_end <='		=> date('Y-m-d') . ' 23:59:00'
							,'user_type'			=> $this->config->item('IND_EMPLOYEE')
							,'user_id'				=> $adm->user_id
					];
					$events = $this->calendar->getEventsParticipant($company->company_id, $local->local_id, $where);
					if(count($events)>= 0x0001) {
						$admin_content .= "<br />\n== CITAS EN EL LOCAL $local->local_name ==<br />\n";
					}
					foreach($events as $event) {
						$cust = $this->db->get_where('rel_quote_participant', ['quote_id' => $event->quote_id, 'user_type' => $this->config->item('IND_CUSTOMER')])->row();
						$customer = $this->db->get_where('gen_user', ['user_id' => $cust->user_id])->row();
						$cliente = $customer->user_firstname . ' ' . $customer->user_lastname;
						$fs = explode(" ", $event->quote_fec_start);
						$fe = explode(" ", $event->quote_fec_end);
						$admin_content .= "******************************<br />\n";
						$admin_content .= "*&ensp;T&Iacute;TULO: &thinsp; $event->quote_title <br />\n";
						$admin_content .= "*&ensp;HORA INICIO: &thinsp; " . converDate($fs[0]) ." $fs[1]<br />\n";
						$admin_content .= "*&ensp;HORA FIN: &thinsp; " . converDate($fe[0]) ." $fe[1]<br />\n";
						$admin_content .= "*&ensp;ATENCION AL CLIENTE: &thinsp; " . $cliente . "<br />\n";
					}
					if(count($events)>= 0x0001) {
						$admin_content .= "******************************<br /><br />";
						$msg = 'Y';
					}
				}
			}
			$admin_content .= "<br />";
			if($msg == 'Y') {
				if(!filter_var($admin_email, FILTER_VALIDATE_EMAIL)) {
				
				} else {
					$this->_sendEmail($admin_email, $this->config->item('EMAIL_USER'), $subject_admin, $admin_content);
				}
			}
		}
	}
	
	public function cronUserTomorrowEvent() {
		$this->db->from('rel_company_employee');
		$this->db->join('gen_user', 'gen_user.user_id=rel_company_employee.user_id');
		$this->db->group_by('rel_company_employee.user_id');
		$admins = $this->db->get()->result();
		foreach($admins as $adm) {
			$msg = 'N';
			$admin_content = '';
			//$content_admin .= "&ensp;ADMIN: &thinsp; $adm->user_login <br />\n";
			$admin_email = $adm->user_email;
			$this->db->from('rel_company_employee');
			$this->db->join('jyc_company', 'jyc_company.company_id=rel_company_employee.company_id');
			$this->db->where('rel_company_employee.user_id', $adm->user_id);
			$companies = $this->db->get()->result();
			foreach($companies as $company) {
				$subject_admin = "**Alertas** Citas para ma�ana en $company->company_social \n";
				$admin_content .= "&ensp;&emsp;EMPRESA: &thinsp; $company->company_social <br />\n";
				$admin_content .= "&ensp;&emsp;TRABAJADOR: &thinsp; $adm->user_firstname $adm->user_lastname <br />\n";
				$this->db->from('jyc_local');
				$this->db->where('company_id', $company->company_id);
				$locales = $this->db->get()->result();
				foreach($locales as $local) {
					//$admin_content .= "&ensp;&emsp;&emsp;LOCAL: &thinsp; $local->local_name <br />\n";
					$list_today_events = array();
					$_fs = date('Y-m-d') . '00:00:00';
					$_fe = date('Y-m-d') . ' 23:59:00';
					$where = ['quote_fec_start >='	=> date('Y-m-d H:i:s', strtotime($_fs . '+1 day'))
							,'quote_fec_end <='		=> date('Y-m-d H:i:s', strtotime($_fe . '+1 day'))
							,'user_type'			=> $this->config->item('IND_EMPLOYEE')
							,'user_id'				=> $adm->user_id
					];
					$events = $this->calendar->getEventsParticipant($company->company_id, $local->local_id, $where);
					if(count($events)>= 0x0001) {
						$admin_content .= "<br />\n== CITAS EN EL LOCAL $local->local_name ==<br />\n";
					}
					foreach($events as $event) {
						$cust = $this->db->get_where('rel_quote_participant', ['quote_id' => $event->quote_id, 'user_type' => $this->config->item('IND_CUSTOMER')])->row();
						$customer = $this->db->get_where('gen_user', ['user_id' => $cust->user_id])->row();
						$cliente = $customer->user_firstname . ' ' . $customer->user_lastname;
						$fs = explode(" ", $event->quote_fec_start);
						$fe = explode(" ", $event->quote_fec_end);
						$admin_content .= "******************************<br />\n";
						$admin_content .= "*&ensp;T&Iacute;TULO: &thinsp; $event->quote_title <br />\n";
						$admin_content .= "*&ensp;HORA INICIO: &thinsp; " . converDate($fs[0]) ." $fs[1]<br />\n";
						$admin_content .= "*&ensp;HORA FIN: &thinsp; " . converDate($fe[0]) ." $fe[1]<br />\n";
						$admin_content .= "*&ensp;ATENCION AL CLIENTE: &thinsp; " . $cliente . "<br />\n";
					}
					if(count($events)>= 0x0001) {
						$admin_content .= "******************************<br /><br />";
						$msg = 'Y';
					}
				}
			}
			$admin_content .= "<br />";
			if($msg == 'Y') {
				if(!filter_var($admin_email, FILTER_VALIDATE_EMAIL)) {
	
				} else {
					$this->_sendEmail($admin_email, $this->config->item('EMAIL_USER'), $subject_admin, $admin_content);
				}
			}
		}
	}
	
	private function _compileBody($content, $tpl, $string = NULL) {
		if(is_null($string)) {
			$return = $this->parser->parse($tpl, $content, TRUE);
		} else {
			$return = $this->parser->parse_string($tpl, $content, TRUE);
		}
		
		return $return;
	}
	
	private function _sendEmail($to, $from, $subject, $body) {
		$mail = new PHPMailer();
		$mail->SetLanguage('es');
		
		$mail->IsSMTP();
		//$mail->SMTPSecure = 'tls';
		$mail->Debugoutput = 'html';
		$mail->SMTPDebug = $this->config->item('EMAIL_DEBUG');
		
		$mail->SMTPAuth = true;
		$mail->Host = $this->config->item('EMAIL_SMTP');
		$mail->Port = $this->config->item('EMAIL_PORT');
		$mail->Username = $this->config->item('EMAIL_USER');
		$mail->Password = $this->config->item('EMAIL_PASSWD');
		
		$mail->FromName = $from;
		$mail->From = $from;
		$mail->Subject = $subject;
		$mail->AddAddress($to);
		$mail->Body = $body;
		$mail->IsHTML(true);
		if(!$mail->Send()) {
			$this->output
			->set_content_type('text/html')
			->set_output("Mailer Error: " . $mail->ErrorInfo);
		} else {
			$this->output
			->set_content_type('text/html')
			->set_output("Message sent!");
		}
	}
}