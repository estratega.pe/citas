<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class genModel extends MX_Controller {
	
    function __construct()
    {
        parent::__construct();
    }
    
    function getMaxID($id, $table) {
    	$this->db->select_max($id);
    	return $this->db->get($table)->row()->$id;
    }
    
    function getData($table = null, $where = null, $like = null, $limit = null, $start = null) {
    	if(is_null($table)) {
    		return FALSE;
    	}
    	if(!is_null($where) AND is_array($where))
    		$this->db->where($where);
    	if(!is_null($like) AND is_array($like))
    		$this->db->like($like);
    	if(!is_null($start) AND !is_null($limit))
    		$this->db->limit($limit, $start);
    	return $this->db->get($table)->result();
    }
    
    function getRow($table = null ,$where = null) {
    	if( is_null($table) || is_null($where) ) {
    		return FALSE;
    	}
    	return $this->db->get_where($table, $where)->row();
    }
	#saveData
	function saveData($table = null, $data) {
		if(is_null($table)) {
			return FALSE;
		}
		$this->db->trans_start();
		$this->db->insert($table, $data);
		$ID = $this->db->insert_id();
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return $ID;
	}
	
	function updateData($table = null, $data, $where) {
		if(is_null($table)) {
			return FALSE;
		}
		$this->db->trans_start();
		$this->db->update($table, $data, $where);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function deleteData($table = null, $where) {
		if(is_null($table)) {
			return FALSE;
		}
		$this->db->trans_start();
		$this->db->delete($table, $where);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
			return FALSE;
		else
			return TRUE;
	}
	
	function getTotalData($table = null,$where = null, $like = null, $start = null, $limit = null) {
		if(is_null($table)) {
			return FALSE;
		}
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		if(!is_null($like) AND is_array($like))
			$this->db->like($like);
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($start, $limit);
		$this->db->from($table);
		return $this->db->count_all_results();
	}
}
