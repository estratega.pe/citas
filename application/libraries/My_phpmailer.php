<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
 
class MY_Phpmailer {
    public function MY_Phpmailer() {
    	require_once('PHPMailer/class.smtp.php');
    	require_once('PHPMailer/class.phpmailer.php');
    	require_once 'PHPMailer/PHPMailerAutoload.php';
    }
}