<?php
defined('BASEPATH') OR exit('No direct script access allowed');

#Construye la pagina
if ( ! function_exists('renderPage')) {
	function renderPage($data = 'X') {
		if($data == 'X')
			return false;
			$CI = & get_instance();
			$user_name = (count($CI->session->userdata('USER')) >= 0x0001) ? $CI->session->userdata('USER')->user_email : 'ANONIMO';
			$menu = '';//$CI->parser->parse('menu', ['BASE_URL' => base_url()], TRUE);
			$CI->parser->parse('layout', ['BASE_URL' => base_url(),
					'CONTENT_HEADER' => $CI->parser->parse('header', $data['HEADER'], TRUE)
					,'CONTENT_HEADER_TOP'	=> $CI->parser->parse('header_top', ['BASE_URL' => base_url(), 'USER_NAME' => $user_name], TRUE)
					,'CONTENT_ASIDE_MENU'	=> $CI->parser->parse('menu_' . $CI->session->userdata('USER')->user_type, ['BASE_URL' => base_url()], TRUE)
					//,'CONTENT_MENU'			=> $menu
					,'CONTENT_BODY'			=> $data['BODY']
					,'BODY_TITLE'			=> (isset($data['BODY_TITLE'])) ? $data['BODY_TITLE'] : ''
					,'FOOTER_EXTRA'			=> (isset($data['FOOTER_EXTRA'])) ? $data['FOOTER_EXTRA'] : ''
			]);
	}
}


#Rellena de ceros
if ( ! function_exists('zerofill')) {
	function zerofill ($num, $zerofill = 4)
	{
		return str_pad($num, $zerofill, '0', STR_PAD_LEFT);
	}
}

#Crea link
if ( ! function_exists('createLink')) {
	function createLink($target, $color, $icon, $text, $with = null) {
		if(is_null($with))
			$return =  '<a href="' . $target . '" title="' . $text . '"><span class="btn ' . $color . '"><i class="glyph-icon ' . $icon . '"></i></span></a>';
		else
			$return  = '<a href="' . $target . '"><span class="btn ' . $color . '"><i class="glyph-icon ' . $icon . '"></i>&nbsp;' . $text . '</span></a>';
		return $return;
	}
}

#Crea Button Submit
if ( ! function_exists('createSubmitButton')) {
	function createSubmitButton($text, $color, $_icon = 'X') {
		$icon = '';
		if($_icon != 'X')
			$icon = '<i class="glyph-icon ' . $_icon . '"></i>&nbsp';
		return '<button class="btn ' . $color . '">' . $icon . $text . '</button>';
	}
}

#Formatea la fecha para guardar en DB
if ( ! function_exists('converDate')) {
	function converDate ($fecha, $limitador = '-')
	{
		$f = explode($limitador, $fecha);
		return $f[2] . '-' . $f[1] . '-' . $f[0];
	}
}

#Formatea la feha para mostrarla al usuario
if ( ! function_exists('revertDate')) {
	function revertDate ($fecha, $limitador = '-')
	{
		$f = explode($limitador, $fecha);
		return $f[0] . '-' . $f[1] . '-' . $f[2];
	}
}

#Obtiene Registro
if ( ! function_exists('getRow')) {
	function getRow($table, $where) {
		$CI = & get_instance();
		$CI->db->select('*');
		$CI->db->from($table);
		$CI->db->where($where);
		return $CI->db->get()->row();
	}
}

#Obtiene Registro gen_select por ID
if ( ! function_exists('getRowSelect')) {
	function getRowSelect($ID) {
		$CI = & get_instance();
		$CI->db->select('*');
		$CI->db->from('gen_select');
		$CI->db->where('select_id', $ID);
		return $CI->db->get()->row();
	}
}

#Obtiene el User ID del admin
if ( ! function_exists('getAdminUID')) {
	function getAdminUID() {
		$CI = & get_instance();
		$CI->db->from('rel_company_user');
		$CI->db->where(['company_id' => $CI->session->userdata('USER')->company_id
				,'user_type' => $CI->config->item('IND_ADMIN')
		]);
		return $CI->db->get()->row()->user_id;
	}
}
#Crea thumbnail
if ( ! function_exists('createThumb')) {
	function createThumb($source, $height, $width) {
		$CI = & get_instance();
		$config['image_library'] = 'gd2';
		$config['source_image'] = $source;
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width']         = $width;
		$config['height']       = $height;
		
		$CI->load->library('image_lib', $config);
		
		$CI->image_lib->resize();
	}
}

#Envia Emails
if ( ! function_exists('enviaEmail')) {
	function enviaEmail($to, $from, $subject, $body) {
		$CI = & get_instance();
		$mail = new PHPMailer();
		$mail->SetLanguage('es');
	
		$mail->IsSMTP();
		//$mail->SMTPSecure = 'tls';
		$mail->Debugoutput = 'html';
		$mail->SMTPDebug = $CI->config->item('EMAIL_DEBUG');
	
		$mail->SMTPAuth = true;
		$mail->Host = $CI->config->item('EMAIL_SMTP');
		$mail->Port = $CI->config->item('EMAIL_PORT');
		$mail->Username = $CI->config->item('EMAIL_USER');
		$mail->Password = $CI->config->item('EMAIL_PASSWD');
	
		$mail->FromName = $from;
		$mail->From = $from;
		$mail->Subject = $subject;
		$mail->AddAddress($to);
		$mail->Body = $body;
		$mail->IsHTML(true);
		if(!$mail->Send()) {
			return FALSE;
		} else {
			return TRUE;
		}
	}
}