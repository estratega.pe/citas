<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * SELECT TYPE USER
 */
$config['SELECT_TYPE_USER'] = 1;
/**
 * SELECT_TYPE_DOC
 */
$config['SELECT_TYPE_DOC'] = 2;
/**
 * SELECT_TYPE_QUOTE
 */
$config['SELECT_TYPE_QUOTE'] = 3;
/**
 * SELECT_TYPE_SEX
 */
$config['SELECT_TYPE_SEX'] = 4;




/**
 * ID ADMIN
 */
$config['IND_ADMIN'] = 1;

/**
 * IND TRABAJADOR
 */
$config['IND_EMPLOYEE'] = 2;

/**
 * IND_CUSTOMER
 */
$config['IND_CUSTOMER'] = 3;

/**
 * Email DEBUG
 */
$config['EMAIL_DEBUG'] = 2;
/**
 * Email SMTP
 */
$config['EMAIL_SMTP'] = 'jyconsultores.com';

/**
 * Email PORT
 */
$config['EMAIL_PORT'] = 25;

/**
 * Email USER
 */
$config['EMAIL_USER'] = 'alertas@jyconsultores.com';

/**
 * Email PASWD
 */
$config['EMAIL_PASSWD'] = '/*Alertsquote2016*/';





/**
 * Tipos Sexo
 */
$config['TIPOS_SEXO'] = ['M' => 'MASCULINO', 'F' => 'FEMENINO'];

/**
 * Opciones SI/NO
 */
$config['OPTIONS_SINO'] = [0 => 'NO', 1 => 'SI'];

/**
 * CONFIG UPLOAD
 **/
$config['FOLDER_UPLOAD'] = APPPATH . '../assets/uploads/';

/**
 * CONFIG IMAGE UPLOAD
 */
$config['IMAGE_ALLOW'] = 'jpg|jpeg|png|pneg';
$config['IMAGE_MAX_SIZE'] = 5120;
$config['IMAGE_MAX_WIDTH'] = 1500;
$config['IMAGE_MAX_HEIGHT'] = 1500;
$config['IMAGE_ENCRYPT_NAME'] = TRUE;
