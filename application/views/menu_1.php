<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!-- BEGIN SIDEBAR MENU -->
<ul class="sidebar-menu">
	<li class="sub-menu">
		<a class="" href="{BASE_URL}">
			<i class="icon-dashboard"></i>
			<span>Dashboard</span>
		</a>
	</li>
	<li class="sub-menu">
		<a href="javascript:;" class="">
			<i class="icon-user"></i>
			<span>CUENTA</span>
			<span class="arrow"></span>
		</a>
		<ul class="sub">
			<li><a class="" href="{BASE_URL}user">Mi Cuenta</a></li>
			<li><a class="" href="{BASE_URL}login/loginOut">Desconectar</a></li>
		</ul>
	</li>
	<li class="sub-menu">
		<a href="javascript:;" class="">
			<i class="icon-tasks"></i>
			<span>GENERAL</span>
			<span class="arrow"></span>
		</a>
		<ul class="sub">
			<li><a class="" href="{BASE_URL}company">Empresas</a></li>
			<li><a class="" href="{BASE_URL}local">Locales</a></li>
			<li><a class="" href="{BASE_URL}space">Espacios</a></li>
			<li><a class="" href="{BASE_URL}employee">Trabajadores</a></li>
			<li><a class="" href="{BASE_URL}customer">Clientes</a></li>
			<li><a class="" href="{BASE_URL}calendar">Calendario</a></li>
			<!-- <li><a class="" href="{BASE_URL}">AAAA</a></li>-->
		</ul>
	</li>

</ul>
<!-- END SIDEBAR MENU -->