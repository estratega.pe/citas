<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
	<!-- BEGIN HEAD -->
	<head>
		<meta charset="utf-8" />
		
		{CONTENT_HEADER}
		
		<meta content="width=device-width, initial-scale=1.0" name="viewport" />
		<meta content="" name="description" />
		<meta content="lgonzales@jyconsultores.com" name="author" />
		<link href="{BASE_URL}assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<link href="{BASE_URL}assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
		<link href="{BASE_URL}assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
		<link href="{BASE_URL}assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
		<link href="{BASE_URL}css/style.css" rel="stylesheet" />
		<link href="{BASE_URL}css/style-responsive.css" rel="stylesheet" />
		<link href="{BASE_URL}css/style-default.css" rel="stylesheet" id="style_color" />
		
		<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/uniform/css/uniform.default.css" />
		
		<link href="{BASE_URL}assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
		
		<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/bootstrap-timepicker/compiled/timepicker.css" />
		<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/bootstrap-datepicker/css/datepicker.css" />
		<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/jquery-ui-1.10.4.custom/css/smoothness/jquery-ui-1.10.4.custom.min.css" />
		<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />-->
		
		<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/jQuery-Autocomplete-master/content/styles.css" />
		
		
	</head>
	<!-- END HEAD -->
	<!-- BEGIN BODY -->
	<body class="fixed-top">
		{CONTENT_HEADER_TOP}
		<!-- BEGIN CONTAINER -->
		<div id="container" class="row-fluid">
			<!-- BEGIN SIDEBAR -->
			<div class="sidebar-scroll">
				<div id="sidebar" class="nav-collapse collapse">
				
					<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
					<div class="navbar-inverse">
					<!-- <form class="navbar-search visible-phone">
					<input type="text" class="search-query" placeholder="Search" />
					</form> -->
					</div>
					<!-- END RESPONSIVE QUICK SEARCH FORM -->
					{CONTENT_ASIDE_MENU}
				</div>
			</div>
			<!-- END SIDEBAR -->
			<!-- BEGIN PAGE -->
			<div id="main-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
	            <div class="row-fluid">
	               <div class="span12">
	                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
	                   <h3 class="page-title">
	                       {BODY_TITLE}
	                   </h3>
	                   <!-- <ul class="breadcrumb">
	                       <li>
	                           <a href="#">Home</a>
	                           <span class="divider">/</span>
	                       </li>
	                       <li>
	                           <a href="#">Form Stuff</a>
	                           <span class="divider">/</span>
	                       </li>
	                       <li class="active">
	                           Form Validation
	                       </li>
	                       <li class="pull-right search-wrap">
	                           <form action="search_result.html" class="hidden-phone">
	                               <div class="input-append search-input-area">
	                                   <input class="" id="appendedInputButton" type="text">
	                                   <button class="btn" type="button"><i class="icon-search"></i> </button>
	                               </div>
	                           </form>
	                       </li>
	                   </ul> -->
	                   <!-- END PAGE TITLE & BREADCRUMB-->
	               </div>
	            </div>
	            {CONTENT_BODY}
	            <!-- END PAGE CONTENT-->
	         </div>
	         <!-- END PAGE CONTAINER-->
			</div>
			<!-- END PAGE -->
		</div>
		<!-- END CONTAINER -->
	
		<!-- BEGIN FOOTER -->
		<div id="footer">
	       2016 - <?php echo date('Y');?> &copy; lgonzales.
		</div>
		<!-- END FOOTER -->
	
		<!-- BEGIN JAVASCRIPTS -->
		<!-- Load javascripts at bottom, this will reduce page load time -->
		<script src="{BASE_URL}js/jquery-1.8.3.min.js"></script>
		<script src="{BASE_URL}js/jquery.nicescroll.js" type="text/javascript"></script>
		<script type="text/javascript" src="{BASE_URL}assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>
		<script type="text/javascript" src="{BASE_URL}assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
		<script src="{BASE_URL}assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
		<script src="{BASE_URL}assets/bootstrap/js/bootstrap.min.js"></script>
		<script src="{BASE_URL}js/jquery.scrollTo.min.js"></script>
		
		<script type="text/javascript" src="{BASE_URL}assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="{BASE_URL}assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
		<script type="text/javascript" src="{BASE_URL}assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
		<script type="text/javascript" src="{BASE_URL}assets/bootstrap/js/bootstrap-fileupload.js"></script>
		
		<!-- ie8 fixes -->
		<!--[if lt IE 9]>
		<script src="{BASE_URL}js/excanvas.js"></script>
		<script src="{BASE_URL}js/respond.js"></script>
		<![endif]-->
		

		
		<script src="{BASE_URL}assets/widgets/jQuery-Autocomplete-master/scripts/jquery.mockjax.js"></script>
		<script src="{BASE_URL}assets/widgets/jQuery-Autocomplete-master/src/jquery.autocomplete.js"></script>
		<!--common script for all pages-->
		
		<script src="{BASE_URL}js/common-scripts.js"></script>
		
		<!-- END JAVASCRIPTS -->
		{FOOTER_EXTRA}
	</body>
	<!-- END BODY -->
</html>