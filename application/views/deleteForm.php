<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
			<div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN VALIDATION STATES-->
                    <div class="widget {BG_COLOR}">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> {BODY_SUBTITLE}</h4>
                        </div>
                        <div class="widget-body form">
                            <!-- BEGIN FORM-->
                            <form class="cmxform form-horizontal" id="signupForm" method="post" action="{URL_POST}">
                                <div class="control-group ">
                                    {BODY_DESCRIPTION} {INPUT_DELETE_ID}
                                </div>

                                <div class="form-actions">
                                    {BUTTON_SUBMIT}
                                    {BUTTON_CANCEL}
                                </div>

                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END VALIDATION STATES-->
                </div>
			</div>