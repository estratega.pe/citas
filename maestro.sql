INSERT INTO `gen_category` (`category_id`, `category_name`) VALUES
(1, 'USUARIO'),
(2, 'DOCUMENTO'),
(3, 'TIPO ACTIVIDAD CITA'),
(4, 'SEXO');

INSERT INTO `gen_select` (`select_id`, `select_name`, `category_id`) VALUES
(1, 'ADMIN', 1),
(2, 'TRABAJADOR', 1),
(3, 'CLIENTE', 1),
(4, 'DNI', 2),
(5, 'PASAPORTE', 2),
(6, 'CARNET DE EXTRANJERIA', 2),
(7, 'RUC', 2),
(8, 'LLAMADA', 3),
(9, 'EMAIL', 3),
(10, 'NOTA', 3),
(11, 'HOMBRE', 4),
(12, 'MUJER', 4);